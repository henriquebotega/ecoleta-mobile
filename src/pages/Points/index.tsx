import React, { useState, useEffect } from "react";
import {
	View,
	Text,
	TouchableOpacity,
	Image,
	ScrollView,
	Alert,
} from "react-native";
import styles from "./styles";
import { Feather as Icon } from "@expo/vector-icons";
import { useNavigation, useRoute } from "@react-navigation/native";
import MapView, { Marker } from "react-native-maps";
import { SvgUri } from "react-native-svg";
import api from "../../services/api";
import * as Location from "expo-location";

interface Item {
	id: number;
	title: string;
	image_url: string;
}

interface Point {
	id: number;
	name: string;
	image: string;
	image_url: string;
	latitude: number;
	longitude: number;
}

interface Params {
	uf: string;
	city: string;
}

const Points = () => {
	const navigation = useNavigation();
	const route = useRoute();

	const routeParams = route.params as Params;

	const [items, setItems] = useState<Item[]>([]);
	const [points, setPoints] = useState<Point[]>([]);

	const [selectedItems, setSelectedItems] = useState<number[]>([]);

	const [initialPosition, setInitialPosition] = useState<[number, number]>([
		0,
		0,
	]);

	useEffect(() => {
		async function loadPosition() {
			const { status } = await Location.requestPermissionsAsync();

			if (status !== "granted") {
				Alert.alert(
					"Ops...",
					"Precisamos de sua permissao para obter sua localizacao"
				);
				return;
			}

			const location = await Location.getCurrentPositionAsync();
			const { latitude, longitude } = location.coords;
			setInitialPosition([latitude, longitude]);
		}

		loadPosition();
	}, []);

	useEffect(() => {
		api.get("/items").then((res) => {
			setItems(res.data);
		});
	}, []);

	useEffect(() => {
		api
			.get("/points", {
				params: {
					city: routeParams.city,
					uf: routeParams.uf,
					items: selectedItems.toString(),
				},
			})
			.then((res) => {
				setPoints(res.data);
			});
	}, [selectedItems]);

	function handleSelectItem(id: number) {
		const alreadySelected = selectedItems.findIndex((i) => i === id);

		if (alreadySelected > -1) {
			const filteredItems = selectedItems.filter((i) => i !== id);
			setSelectedItems(filteredItems);
		} else {
			setSelectedItems([...selectedItems, id]);
		}
	}

	function handleNavigateBack() {
		navigation.goBack();
	}

	function handleNavigateToDetail(id: number) {
		navigation.navigate("Detail", { point_id: id });
	}

	return (
		<>
			<View style={styles.container}>
				<TouchableOpacity onPress={handleNavigateBack}>
					<Icon name="arrow-left" size={20} color="#34cb79" />
				</TouchableOpacity>

				<Text style={styles.title}>Bem vindo</Text>
				<Text style={styles.description}>
					Encontre no mapa um ponto de coleta
				</Text>

				<View style={styles.mapContainer}>
					{initialPosition[0] !== 0 && (
						<MapView
							style={styles.map}
							initialRegion={{
								latitude: initialPosition[0],
								longitude: initialPosition[1],
								latitudeDelta: 0.014,
								longitudeDelta: 0.014,
							}}
						>
							{points.map((i) => {
								return (
									<Marker
										key={String(i.id)}
										onPress={() => handleNavigateToDetail(i.id)}
										style={styles.mapMarker}
										coordinate={{
											latitude: i.latitude,
											longitude: i.longitude,
										}}
									>
										<View style={styles.mapMarkerContainer}>
											<Image
												source={{ uri: i.image_url }}
												style={styles.mapMarkerImage}
											/>
											<Text style={styles.mapMarkerTitle}>{i.name}</Text>
										</View>
									</Marker>
								);
							})}
						</MapView>
					)}
				</View>
			</View>

			<View style={styles.itemsContainer}>
				<ScrollView
					horizontal
					showsHorizontalScrollIndicator={false}
					contentContainerStyle={{ paddingHorizontal: 20 }}
				>
					{items.map((i) => (
						<TouchableOpacity
							key={String(i.id)}
							style={[
								styles.item,
								selectedItems.includes(i.id) ? styles.selectedItem : {},
							]}
							activeOpacity={0.6}
							onPress={() => handleSelectItem(i.id)}
						>
							<SvgUri width={42} height={42} uri={i.image_url} />
							<Text style={styles.itemTitle}>{i.title}</Text>
						</TouchableOpacity>
					))}
				</ScrollView>
			</View>
		</>
	);
};

export default Points;
