import React, { useState, useEffect } from "react";
import { View, ImageBackground, Text, Image } from "react-native";
import styles from "./styles";
import { RectButton } from "react-native-gesture-handler";
import { Feather as Icon } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";
import axios from "axios";
import RNPickerSelect from "react-native-picker-select";

interface UF {
	label: string;
	value: string;
}

interface UfIBGE {
	sigla: string;
}

interface CityIBGE {
	nome: string;
}

interface City {
	label: string;
	value: string;
}

const Home = () => {
	const navigation = useNavigation();
	const [ufs, setUfs] = useState<UF[]>([]);
	const [cities, setCities] = useState<City[]>([]);
	const [selectedUF, setSelectedUF] = useState("0");
	const [selectedCity, setSelectedCity] = useState("0");

	useEffect(() => {
		axios
			.get(
				"https://servicodados.ibge.gov.br/api/v1/localidades/estados?orderBy=nome"
			)
			.then((res) => {
				const siglas = res.data.map((uf: UfIBGE) => {
					return {
						label: uf.sigla,
						value: uf.sigla,
					};
				});
				setUfs(siglas);
			});
	}, []);

	useEffect(() => {
		if (selectedUF !== "0") {
			axios
				.get(
					`https://servicodados.ibge.gov.br/api/v1/localidades/estados/${selectedUF}/municipios`
				)
				.then((res) => {
					const cidades = res.data.map((city: CityIBGE) => {
						return {
							label: city.nome,
							value: city.nome,
						};
					});
					setCities(cidades);
				});
		}
	}, [selectedUF]);

	function handleSelectedCity(value: string) {
		setSelectedCity(value);
	}

	function handleSelectedUF(value: string) {
		setSelectedUF(value);
	}

	function handleNavigateToPoints() {
		navigation.navigate("Points", { uf: selectedUF, city: selectedCity });
	}

	return (
		<ImageBackground
			source={require("../../assets/home-background.png")}
			imageStyle={{ width: 274, height: 368 }}
			style={styles.container}
		>
			<View style={styles.main}>
				<Image source={require("../../assets/logo.png")} />
				<Text style={styles.title}>Seu marketplace de coleta de residuos</Text>
				<Text style={styles.description}>
					Ajudamos pessoas a encontrarem pontos de coleta de forma eficiente.
				</Text>
			</View>

			<View style={styles.footer}>
				<RNPickerSelect onValueChange={handleSelectedUF} items={ufs} />
				<RNPickerSelect onValueChange={handleSelectedCity} items={cities} />

				<RectButton style={styles.button} onPress={handleNavigateToPoints}>
					<View style={styles.buttonIcon}>
						<Text>
							<Icon size={24} color="#fff" name="arrow-right" />
						</Text>
					</View>
					<Text style={styles.buttonText}>Entrar</Text>
				</RectButton>
			</View>
		</ImageBackground>
	);
};

export default Home;
